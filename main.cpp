#include <iostream>

using namespace std;

class book {
    int n_id{}, publish_year{};
    string title, author, publisher;
public:
    void assign(int a, string b, string c, string d, int e);
    void display();
};

int main() {
    book b1, b2, b3, b4, b5;
    b1.assign(1, "Krew elfow", "Andrzej Sapkowski", "superNOWA", 1994);
    b2.assign(2, "Czas pogardy", "Andrzej Sapkowski", "superNOWA", 1995);
    b3.assign(3, "Chrzest ognia", "Andrzej Sapkowski", "superNOWA", 1996);
    b4.assign(4, "Wieza jaskolki", "Andrzej Sapkowski", "superNOWA", 1997);
    b5.assign(5, "Pani jeziora", "Andrzej Sapkowski", "superNOWA", 1999);
    b1.display();
    b2.display();
    b3.display();
    b4.display();
    b5.display();
    return 0;
}

void book::assign(int a, string b, string c, string d, int e) {
    n_id = a;
    title = move(b);
    author = move(c);
    publisher = move(d);
    publish_year = e;
}

void book::display() {
    cout << "ID: " << n_id << "\n"
         << "Tytul: " << title << "\n"
         << "Autor: " << author << "\n"
         << "Wydawnictwo: " << publisher << "\n"
         << "Data wydania: " << publish_year << "\n";
}
